<?php

return [
    'file_min_lifetime_sec' => env('FILE_MIN_LIFETIME_SEC'),
    'file_max_lifetime_sec' => env('FILE_MAX_LIFETIME_SEC'),
    'file_max_size_kb' => env('FILE_MAX_SIZE'),
    'confirmed_account_max_size' => env('CONF_ACC_MAX_SIZE'),
    'not_confirmed_account_max_size' => env('NOT_CONF_ACC_MAX_SIZE'),
    'files_directory' => 'user_files',
    'user_share_limit' => env('USER_SHARE_LIMIT'),
];
