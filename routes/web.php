<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/* @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->group(['prefix' => '/api/v1'], function () use ($router) {

    //auth
    $router->group(['prefix' => '/auth'], function () use ($router) {
        $router->post('register', 'AuthController@registerAction');
        $router->post('login', 'AuthController@loginAction');
        $router->post('change-password', 'AuthController@changePasswordAction');
    });

    //directories
    $router->group(['middleware' => 'auth:api'], function () use ($router) {
        $router->group(['prefix' => '/directories'], function () use ($router) {
            $router->get('/', 'DirectoriesController@listAction');
            $router->post('/', 'DirectoriesController@createAction');
            $router->get('/{directoryUuid}', 'DirectoriesController@getAction');
            $router->put('/{directoryUuid}', 'DirectoriesController@updateAction');
            $router->delete('/{directoryUuid}', 'DirectoriesController@deleteAction');

            //invite
            $router->post('/{directoryUuid}/share', 'DirectoriesController@shareAction');
            $router->delete('/{directoryUuid}/leave', 'DirectoriesController@leaveAction');
        });

        //files
        $router->group(['prefix' => '/directories/{directoryUuid}/files'], function () use ($router) {
            $router->get('/', 'FilesController@listAction');
            $router->post('/', 'FilesController@uploadAction');
            $router->put('/{fileUuid}', 'FilesController@editAction');
            $router->delete('/{fileUuid}', 'FilesController@deleteAction');
            $router->post('/{fileUuid}/move/{directoryUuidMove}', 'FilesController@moveAction');
            $router->post('/{fileUuid}/cope/{directoryUuidCopy}', 'FilesController@copyAction');
        });
    });
});
