<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Invite
 * @package App\Models
 */
class Invite extends Model
{

    /**
     * @return BelongsTo
     */
    public function users(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsToMany
     */
    public function directories(): BelongsToMany
    {
        return $this->belongsToMany(
            Directory::class,
            'invites_shared_directories',
            'invite_id',
            'directory_id'
        );
    }
}
