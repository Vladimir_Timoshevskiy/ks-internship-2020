<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Directory extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'uuid',
    ];

    public $timestamps = false;

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class, 'users_directories', 'directories_id', 'user_id')
            ->withPivot('is_master');
    }

    /**
     * @return User|null
     */
    public function ownerUser(): ?User
    {
        return $this->users()->where('is_master', true)->first();
    }

    /**
     * @return BelongsToMany
     */
    public function files(): BelongsToMany
    {
        return $this
            ->belongsToMany(File::class, 'files_directories', 'directories_id', 'file_id');
    }

    /**
     * @return BelongsToMany
     */
    public function invites(): BelongsToMany
    {
        return $this->belongsToMany(
            Invite::class,
            'invites_shared_directories',
            'directory_id',
            'invite_id'
        );
    }

    public function isAlreadyShared(): bool
    {
        $invites = $this->invites()->exists();
        $sharedSlaves = $this->users()->wherePivot('is_master', false)->exists();

        return ($invites || $sharedSlaves);
    }
}
