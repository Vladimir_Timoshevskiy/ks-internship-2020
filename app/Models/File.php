<?php


namespace  App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class File extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'filename',
        'name',
        'uuid',
        'lifetime_seconds',
        'file_size'
    ];

    /**
     * @return BelongsToMany
     */
    public function directories(): BelongsToMany
    {
        return $this
            ->belongsToMany(Directory::class, 'files_directories', 'file_id', 'directory_id');
    }
}
