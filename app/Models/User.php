<?php

namespace  App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use \Illuminate\Support\Facades\Cache;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    public const TOTAL_FILES_CACHE_PREFIX = 'Exchanger::totalFiles::';
    public const TOTAL_FILES_SHARED_DIR_PREFIX = 'Exchanger::totalDirectoratesShares::';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password_hash',
        'new_password'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return int
     */
    public function getCurrentFilesSize(): int
    {
        return (int)Cache::get(self::TOTAL_FILES_CACHE_PREFIX.$this->id);
    }

    /**
     * @param int $fileSize
     */
    public function raiseFileSize(int $fileSize): void
    {
        Cache::store('redis')->increment(self::TOTAL_FILES_CACHE_PREFIX.$this->id, $fileSize);
    }

    /**
     * @return int
     */
    public function getCurrentSharedCounter(): int
    {
        return (int)Cache::get(self::TOTAL_FILES_SHARED_DIR_PREFIX, $this->id);
    }

    /**
     * @param bool $increment
     * @return int
     */
    public function changeCurrentSharesCounter(bool $increment): int
    {
        if ($increment) {
            return Cache::store('redis')->increment(self::TOTAL_FILES_SHARED_DIR_PREFIX.$this->id);
        }

        return Cache::store('redis')->decrement(self::TOTAL_FILES_SHARED_DIR_PREFIX.$this->id);
    }

    /**
     * @return BelongsToMany
     */
    public function directories(): BelongsToMany
    {
        return $this
            ->belongsToMany(Directory::class, 'users_directories', 'user_id', 'directories_id')
            ->withPivot('is_master');
    }

    /**
     * @return Directory|null
     */
    public function defaultDirectory(): ?Directory
    {
        return $this->directories()->where('is_master', true)->first();
    }
}
