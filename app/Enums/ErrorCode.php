<?php

namespace App\Enums;

/**
 * Interface ErrorCode
 * @package App\Enums
 */
interface ErrorCode
{
    public const GENERAL_CODE = 1000;

    public const USER_NOT_CONFIRMED = 1001;
    public const USER_NOT_FOUND = 1002;
    public const VALIDATION_FAILED = 2001;

    public const MODEL_NOT_FOUND = 3001;
    public const ACCOUNT_FILESIZE_LIMIT = 3002;
    public const ACCOUNT_LIFETIME_LIMIT = 3003;

    public const DIRECTORY_NOT_OWNED = 4001;
    public const DIRECTORY_SHARED = 4002;
}
