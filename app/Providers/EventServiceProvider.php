<?php

namespace App\Providers;

use App\Events\DirectorySharedEvent;
use App\Events\FileAddedEvent;
use App\Events\UserInviteEvent;
use App\Events\UserRegisteredEvent;
use App\Listeners\StatisticListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
       DirectorySharedEvent::class => [
           StatisticListener::class
       ],

        FileAddedEvent::class => [
            StatisticListener::class
        ],

        UserInviteEvent::class => [
            StatisticListener::class
        ],

        UserRegisteredEvent::class => [
            StatisticListener::class
        ],

    ];
}
