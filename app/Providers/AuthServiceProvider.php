<?php

namespace App\Providers;

use App\Exceptions\BaseAppException;
use App\Exceptions\UserNotFoundException;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    private const TOKEN = "X-API-TOKEN";

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('api', function ($request) {
            /**
             * @var $request Request
             */
            if ($request->hasHeader(self::TOKEN)) {
                $token = $request->header(self::TOKEN);
                try {
                    $user = User::where('api_token', $token)->firstOrFail();

                    return $user;
                } catch (ModelNotFoundException $exception) {
                    throw new UserNotFoundException('User with such token not found.');
                }
            }
            throw new BaseAppException('Auth token was not provided.');
        });
    }
}
