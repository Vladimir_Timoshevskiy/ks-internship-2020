<?php


namespace App\Listeners;

use App\Events\DirectorySharedEvent;

/**
 * Class DirectorySharedListener
 * @package App\Listeners
 */
class DirectorySharedListener extends StatisticListener
{
    /**
     * @param DirectorySharedEvent $event
     */
    public function handler(DirectorySharedEvent $event)
    {
        $data = [
            'owner_id' => $event->getOwner()->id,
            'directory_uuid' => $event->getDirectory()->uuid,
            'invitee_email' => $event->getInvitee()->username,
            'invitee_id' => $event->getInvitee()->id,
        ];

        $this->service->sendMessage($data);
    }
}
