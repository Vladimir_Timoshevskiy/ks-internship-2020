<?php


namespace App\Listeners;

use App\Events\StaticQueueEvent;
use App\Services\QueueService;

/**
 * Class StatisticListener
 * @package App\Listeners
 */

class StatisticListener
{

    protected $service;
    /**
     * StatisticListener constructor.
     * @param QueueService $service
     */
    public function __construct(QueueService $service)
    {
        $this->service = $service;
    }

    public function handler(StaticQueueEvent $event)
    {
        $this->service->sendMessage($event->getDataArray());
    }
}
