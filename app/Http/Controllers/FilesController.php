<?php


namespace App\Http\Controllers;

use App\Models\Directory;
use App\Models\File;
use App\Services\FilesServices;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

/**
 * Class FilesController
 * @package App\Http\Controllers
 */
class FilesController extends Controller
{
    private $filesConfig;
    private $service;
    private $user;

    /**
     * FilesController constructor.
     */
    public function __construct()
    {
        $this->filesConfig = config('exchanger');
        $this->service = new FilesServices();
        $this->user = Auth::user();
    }

    /**
     * @param string $directoryUuid
     * @return JsonResponse
     */
    public function listAction(string $directoryUuid): JsonResponse
    {
        return response()->json(Directory::where('uuid', $directoryUuid)->firstOrFail()->files()->get());
    }

    /**
     * @param string $directoryUuid
     * @param Request $request
     * @throws ValidationException
     * @throws \Throwable
     */
    public function uploadAction(string $directoryUuid, Request $request): void
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'lifetime_seconds' => [
                'max:' . $this->filesConfig['file_max_lifetime_sec'],
                'min:' . $this->filesConfig['file_min_lifetime_sec']
            ],
            'file' => 'required|file|max:' . $this->filesConfig['file_max_size_kb']
        ]);

        /* @var User $user */
        $user = Auth::user();
        /* @var Directory $directory */
        $directory = $user->directories()->where('uuid', $directoryUuid)->firstOrFail();
        /* @var UploadedFile $file */
        $file = $request->file('file');

        $lifetimeSeconds = $request->post('lifetime_seconds');

        $this->service->saveFile($user, $directory, $file, $request->post('filename'), $lifetimeSeconds);
    }

    public function editAction(string $fileUuid, Request $request): void
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $this->user->directories()->files()
            ->where('uuid', $fileUuid)
            ->update(['name' => $request->name]);
    }

    /**
     * @param string $fileUuid
     * @param string $directoryUuidMove
     */
    public function moveAction(string $fileUuid, string $directoryUuidMove): void
    {
        $this->copyAction($fileUuid, $directoryUuidMove);
        $this->user->directories()->files()->firstOrFail()->delete();
    }

    /**
     * @param string $fileUuid
     * @param string $directoryUuidCopy
     */
    public function copyAction(string $fileUuid, string $directoryUuidCopy): void
    {
        $file = $this->user->directories()->files()->where('uuid', $fileUuid)->firstOrFail();
        $directory = Directory::where('uuid', $directoryUuidCopy)->firstOrFail;

        $fileMove = File::create([
            'uuid' => Str::uuid(),
            'name' => $file->name,
            'filename' => $file->filename,
            'lifetime_seconds' => $file->lifetime_seconds,
            'file_size' => $file->file_size,
        ]);

        $fileMove->directories()->attach($directory->id);
    }

    /**
     * @param string $fileUuid
     */
    public function deleteAction(string $fileUuid): void
    {
        File::where('uuid', $fileUuid)->delete();
    }
}
