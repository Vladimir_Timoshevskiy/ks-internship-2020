<?php

namespace App\Http\Controllers;

use App\Models\Directory;
use App\Exceptions\UserNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Mockery\Exception;
use Throwable;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function registerAction(Request $request): JsonResponse
    {
        $this->validate($request, [
            'username' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::create([
            'username' => $request->username,
            'password_hash' => Hash::make($request->password),
            'api_token' => Str::random(32),
        ]);

        $directoryModel = Directory::create([
            'uuid' => Str::uuid(),
            'name' => 'default',
        ]);

        $directoryModel->users()->attach($user->id, ['is_master' => '1']);

        return $this->successResponse($user);
    }

    /**
     * @param Request $request
     * @return \Exception|JsonResponse|Exception
     * @throws ValidationException
     */
    public function loginAction(Request $request): JsonResponse
    {

        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $user = User::where('username', $request->username)->firstOrFail();

        if (Hash::check($request->password, $user->password_hash)) {
            return $this->successResponse($user);
        }

        throw new UserNotFoundException("User not found");
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    public function changePasswordAction(Request $request): void
    {
        $this->validate($request, [
            'token' => 'required|string',
            'old_password' => 'required|string',
            'new_password' => 'required|string',
        ]);

        $user = User::where('api_token', $request->token)->firstOrFail();

        if (Hash::check($request->old_password, $user->password_hash)) {
            $user->password_hash = Hash::make($request->new_password);
            $user->saveOrFail();
        }
    }
}
