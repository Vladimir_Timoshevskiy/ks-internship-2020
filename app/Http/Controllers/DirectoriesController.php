<?php


namespace App\Http\Controllers;

use App\Models\Directory;
use App\Services\ShareDirectoriesServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Throwable;

/**
 * Class DirectoriesController
 * @package App\Http\Controllers
 */
class DirectoriesController extends Controller
{
    private $user;

    /**
     * DirectoriesController constructor.
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * @return JsonResponse
     */
    public function listAction(): JsonResponse
    {
        return response()->json($this->user->directories()->get()->toArray());
    }

    /**
     * @param string $directoryUuid
     * @return JsonResponse
     */
    public function getAction(string $directoryUuid): JsonResponse
    {
        return response()->json($this->user->directories()->get()->where('uuid', $directoryUuid));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createAction(Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $directoryModel = Directory::create([
            'uuid' => Str::uuid(),
            'name' => $request->name,
        ]);

        return response()->json($directoryModel->users()->attach($directoryModel->id, ['is_master' => 1]));
    }

    /**
     * @param string $directoryUuid
     * @param Request $request
     * @throws ValidationException
     */
    public function updateAction(string $directoryUuid, Request $request): void
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $this->user->directories()
            ->where('uuid', $directoryUuid)
            ->update(['name' => $request->name]);
    }

    /**
     * @param string $directoryUuid
     */
    public function deleteAction(string $directoryUuid): void
    {
        $this->user->directories()->where('uuid', $directoryUuid)->delete();
    }

    /**
     * @param Request $request
     * @param string $directoryUuid
     * @return JsonResponse
     * @throws ValidationException
     * @throws Throwable
     */
    public function shareAction(Request $request, string  $directoryUuid): JsonResponse
    {
        $this->validate($request, [
            'invitee_username' => 'required|email|max:255'
        ]);
        $inviteeEmail = $request->post('invitee_username');
        $service = new ShareDirectoriesServices();
        $service->shareDirectory($this->user, $directoryUuid, $inviteeEmail);

        return $this->successResponse([]);
    }

    /**
     * @param string $directoryUuid
     * @return JsonResponse
     */
    public function leaveAction(string  $directoryUuid): JsonResponse
    {
        $service = new ShareDirectoriesServices();
        $service->leaveDirectory($this->user, $directoryUuid);

        return $this->successResponse([]);
    }
}
