<?php


namespace App\Exceptions;

use App\Enums\ErrorCode;
use Illuminate\Http\Response;

/**
 * Class UserNotFoundException
 * @package App\Exceptions
 */
class UserNotFoundException extends BaseAppException
{
    protected $httpStatusCode = Response::HTTP_BAD_REQUEST;
    protected $errorCode = ErrorCode::USER_NOT_FOUND;
}
