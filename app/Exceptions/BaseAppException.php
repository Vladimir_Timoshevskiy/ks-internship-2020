<?php


namespace App\Exceptions;

use App\Enums\ErrorCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Mockery\Exception;

/**
 * Class BaseAppException
 * @package App\Exceptions
 */
class BaseAppException extends Exception
{
    protected $httpStatusCode = Response::HTTP_CONFLICT;
    protected $errorCode = ErrorCode::ACCOUNT_FILESIZE_LIMIT;

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json([
            'success' => false,
            'message' => $this->message,
            'error_code' => $this->errorCode,
        ], $this->httpStatusCode);
    }
}
