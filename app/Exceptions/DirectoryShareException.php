<?php


namespace App\Exceptions;


use App\Enums\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class DirectoryShareException extends BaseAppException
{
    protected $httpStatusCode = Response::HTTP_BAD_REQUEST;
    protected $errorCode = ErrorCode::DIRECTORY_SHARED;
}
