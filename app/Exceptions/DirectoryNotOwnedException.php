<?php


namespace App\Exceptions;

use App\Enums\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DirectoryNotOwnedException
 * @package App\Exceptions
 */
class DirectoryNotOwnedException extends BaseAppException
{
    protected $httpStatusCode = Response::HTTP_BAD_REQUEST;
    protected $errorCode = ErrorCode::DIRECTORY_NOT_OWNED;
}
