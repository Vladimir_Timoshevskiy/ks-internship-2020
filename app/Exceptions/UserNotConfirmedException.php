<?php


namespace App\Exceptions;

use App\Enums\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserNotConfirmedException
 * @package App\Exceptions
 */
class UserNotConfirmedException extends BaseAppException
{
    protected $httpStatusCode = Response::HTTP_BAD_REQUEST;
    protected $errorCode = ErrorCode::USER_NOT_CONFIRMED;
}
