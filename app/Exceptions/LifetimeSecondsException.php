<?php


namespace App\Exceptions;

use App\Enums\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LifetimeSecondsException
 * @package App\Exceptions
 */
class LifetimeSecondsException extends BaseAppException
{
    protected $httpStatusCode = Response::HTTP_BAD_REQUEST;
    protected $errorCode = ErrorCode::ACCOUNT_LIFETIME_LIMIT;
}
