<?php


namespace App\Exceptions;

use App\Enums\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TotalFilesLimitExceptions
 * @package App\Exceptions
 */
class TotalFilesLimitExceptions extends BaseAppException
{
    protected $httpStatusCode = Response::HTTP_BAD_REQUEST;
    protected $errorCode = ErrorCode::ACCOUNT_FILESIZE_LIMIT;
}
