<?php


namespace App\Events;

use App\Models\Invite;
use App\Models\User;

class UserInviteEvent extends Event implements StaticQueueEvent
{

    private $user;
    private $invite;

    /**
     * InviteEvent constructor.
     * @param User $user
     * @param Invite $invite
     */
    public function __construct(User $user, Invite $invite)
    {
        $this->user = $user;
        $this->invite = $invite;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return [
            'user' => $this->user->id,
            'invite' => $this->invite->id
        ];
    }
}
