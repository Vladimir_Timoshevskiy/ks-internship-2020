<?php


namespace App\Events;

interface StaticQueueEvent
{
    public function getDataArray(): array;
}
