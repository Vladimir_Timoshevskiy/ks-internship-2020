<?php

namespace App\Events;

use App\Models\Directory;
use App\Models\User;

class DirectorySharedEvent extends Event implements StaticQueueEvent
{

    private $owner;
    private $directory;
    private $invitee;

    /**
     * Create a new event instance.
     *
     * @param User $owner
     * @param Directory $directory
     * @param User $invitee
     */
    public function __construct(User $owner, Directory $directory, User $invitee)
    {
        $this->owner = $owner;
        $this->directory = $directory;
        $this->invitee = $invitee;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return [
            'owner_id' => $this->owner->id,
            'directory_uuid' => $this->directory->uuid,
            'invitee_email' => $this->invitee->username,
            'invitee_id' => $this->invitee->id,
        ];
    }
}
