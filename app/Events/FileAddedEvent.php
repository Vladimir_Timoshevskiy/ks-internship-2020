<?php


namespace App\Events;

use App\Models\Directory;
use App\Models\File;
use App\Models\User;

/**
 * Class FileAddedEvent
 * @package App\Events
 */
class FileAddedEvent extends Event implements StaticQueueEvent
{

    private $user;
    private $directory;
    private $file;

    public function __construct(User $user, Directory $directory, File $file)
    {
        $this->user = $user;
        $this->directory = $directory;
        $this->file = $file;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return [
            'user' => $this->user->id,
            'directory' => $this->directory->id,
            'file' => $this->file->id
        ];
    }
}
