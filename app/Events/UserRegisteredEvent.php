<?php


namespace App\Events;

use App\Models\User;

class UserRegisteredEvent extends Event implements StaticQueueEvent
{

    private $user;

    /**
     * UserRegisteredEvent constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return ['user' => $this->user->id];
    }
}
