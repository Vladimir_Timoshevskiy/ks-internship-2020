<?php

namespace App\Services;

use App\Events\DirectorySharedEvent;
use App\Exceptions\DirectoryNotOwnedException;
use App\Exceptions\DirectoryShareException;
use App\Exceptions\UserNotConfirmedException;
use App\Models\Directory;
use App\Models\Invite;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Throwable;

/**
 * Class ShareDirectoriesServices
 * @package App\Services
 */
class ShareDirectoriesServices
{

    private $userShareLimit;

    /**
     * ShareDirectoriesServices constructor.
     */
    public function __construct()
    {
        $config = config('exchanger');
        $this->userShareLimit = $config['user_share_limit'];
    }

    /**
     * @param User $owner
     * @param string $directoryUuid
     * @param string $inviteeUsername
     * @throws Throwable
     */
    public function shareDirectory(User $owner, string $directoryUuid, string $inviteeUsername): void
    {
        if (!$owner->confirmed) {
            throw new UserNotConfirmedException('Cannot share!');
        }

        try {
            /** @var Directory $directory */
            $directory = $owner->directories()
               ->wherePivot('is_master', true)
               ->where('directories.uuid', $$directoryUuid)
               ->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            throw new DirectoryNotOwnedException('Directory is not owned or not found');
        }

        $alreadySharedDirectory = $this->checkShareExists($directory);
        if (!$alreadySharedDirectory) {
            $this->checkSharedLimit($owner, $directory);
        }

        $this->checkSelfShared($owner, $inviteeUsername);
        $this->checkSharedSameUser($directory, $inviteeUsername);


        $user = User::query()->where('username', $inviteeUsername)->first();

        if ($user === null) {
            $this->createInvite($owner, $directory, $inviteeUsername);
            $user = new User(['username' => $inviteeUsername]);
        } else {
            $this->linkSharedUser($directory, $user);
        }

        if (!$alreadySharedDirectory) {
            $owner->changeCurrentSharesCounter(true);
        }

        event(new DirectorySharedEvent($owner, $directory, $user));
    }

    /**
     * @param Directory $directory
     * @param string $inviteeUsername
     */
    public function checkSharedSameUser(Directory $directory, string $inviteeUsername): void
    {
        $alreadyShared = $directory
            ->users()
            ->where('username', $inviteeUsername)
            ->wherePivot('is_master', false)
            ->exists();

        $alreadyInviteExists = $directory->invites()->where('invitee_email', $inviteeUsername)->exists();

        if ($alreadyInviteExists || $alreadyShared) {
            throw new DirectoryShareException('Already shared');
        }
    }

    /**
     * @param User $user
     * @param Directory $directory
     */
    public function checkSharedLimit(User $user, Directory $directory): void
    {
        $currentSharesCounter = $user->getCurrentSharedCounter();
        if ($currentSharesCounter >= $this->userShareLimit) {
            throw new DirectoryShareException('Limit exceeded ');
        }
    }

    /**
     * @param Directory $directory
     * @return bool
     */
    public function checkShareExists(Directory $directory): bool
    {
        $sharedExists = $directory->users()->wherePivot('is_master', false)->exists();
        $invitesExists = $directory->invites()->exists();

        return $sharedExists || $invitesExists;
    }

    /**
     * @param User $user
     * @param string $inviteeEmail
     */
    public function checkSelfShared(User $user, string $inviteeEmail): void
    {
        if ($user->username === $inviteeEmail) {
            throw new DirectoryShareException('Cannot shared on self');
        }
    }

    /**
     * @param User $owner
     * @param Directory $directory
     * @param string $inviteeUsername
     * @throws Throwable
     */
    public function createInvite(User $owner, Directory $directory, string $inviteeUsername): void
    {
        $invite = new Invite([
           'invitee_email' => $inviteeUsername,
           'token' => Str::random(),
        ]);

        $invite->users()->associate($owner);
        try {
            DB::beginTransaction();
            $invite->saveOrFail();
            $invite->directories()->attach($directory->id);
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        //$this->mailer->sendInviteEmail($invite);
    }

    public function linkSharedUser(Directory $directory, $user)
    {
    }

    /**
     * @param User $user
     * @param string $directoryUuid
     */
    public function leaveDirectory(User $user, string $directoryUuid): void
    {
        /* @var  Directory $directory*/
        $directory = $user->directories()
            ->wherePivot('is_master', false)
            ->where('uuid', $directoryUuid)
            ->firstOrFail();

        $directory->detach($user->id);

        if ($directory->isAlreadyShared()) {
            return;
        }

        /* @var  User $owner */
        $owner = $directory->users()->wherePivot('is_master', true)->firstOrFail();
        $owner->changeCurrentSharesCounter(false);
    }
}
