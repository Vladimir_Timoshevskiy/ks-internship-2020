<?php


namespace App\Services;

use App\Exceptions\LifetimeSecondsException;
use App\Exceptions\TotalFilesLimitExceptions;
use App\Models\User;
use App\Models\Directory;
use App\Models\File as FileModel;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Throwable;

/**
 * Class FilesServices
 * @package App\Services
 */
class FilesServices
{
    private $minLifetimeSeconds;
    private $maxLifetimeSeconds;
    private $confirmedAccMaxSize;
    private $notConfirmedAccMaxSize;
    private $filesDirectory;

    /**
     * FilesServices constructor.
     */
    public function __construct()
    {
        $config = config('exchanger');
        $this->minLifetimeSeconds = $config['file_min_lifetime_sec'];
        $this->maxLifetimeSeconds = $config['file_max_lifetime_sec'];
        $this->confirmedAccMaxSize = $config['confirmed_account_max_size'];
        $this->notConfirmedAccMaxSize = $config['not_confirmed_account_max_size'];
        $this->filesDirectory = $config['files_directory'];
    }

    /**
     * @param User $user
     * @param Directory $directory
     * @param UploadedFile $file
     * @param string $filename
     * @param int|null $lifetimeSeconds
     * @throws Throwable
     */
    public function saveFile(User $user, Directory $directory, UploadedFile $file, string $filename, int $lifetimeSeconds = null)
    {
        $fileSizeKb = intdiv($file->getSize(), 1024);
        $this->checkUserMaxSize($user, $fileSizeKb);
        $this->checkLifetimeSeconds($lifetimeSeconds);

        $fileModel = new FileModel([
            'name' => $filename,
            'uuid' => Str::uuid(),
            'filename' => $file->getClientOriginalName(),
            'lifetime_seconds' => $lifetimeSeconds,
            'file_size' => $fileSizeKb
        ]);

        $fileModel->saveOrFail();

        $directory->files()->attach($fileModel->id);

        try {
            $file->move($this->filesDirectory, $file->getClientOriginalName());
        } catch (FileException $exception) {
            //do something
        }

        $user->raiseFileSize($fileSizeKb);
    }

    /**
     * @param User $user
     * @param int $newFileSize
     */
    private function checkUserMaxSize(User $user, int $newFileSize): void
    {
        $currentSize = $user->getCurrentFilesSize();
        $newTotalSize = $currentSize + $newFileSize;

        if ($user->confirmed && $newTotalSize > $this->confirmedAccMaxSize) {
            return;
        }

        if (!$user->confirmed && $newTotalSize > $this->notConfirmedAccMaxSize) {
            return;
        }

        throw new TotalFilesLimitExceptions('File total size limit!');
    }

    private function checkLifetimeSeconds(int $lifetimeSeconds)
    {
        if ($lifetimeSeconds > $this->minLifetimeSeconds && $lifetimeSeconds < $this->maxLifetimeSeconds) {
            return;
        }

        throw new LifetimeSecondsException('LifetimeSeconds is incorrect.');
    }
}
