<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->integer('uuid');
            $table->string('name');
            $table->string('filename');
            $table->integer('lifetime_seconds');
            $table->integer('file_size');
            $table->timestamp('created_at')->useCurrent();

        });

        Schema::create('files_directories', function (Blueprint $table) {
            $table->unsignedBigInteger('file_id');
            $table->unsignedBigInteger('directories_id');

            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
            $table->foreign('directories_id')->references('id')->on('directories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
        Schema::dropIfExists('files_directories');
    }
}
