<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('directories', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('name');
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::create('users_directories', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('directories_id');
            $table->boolean('is_master')->default(true);


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('directories_id')->references('id')->on('directories')->onDelete('cascade');

            $table->unique(['user_id', 'directories_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directories');
        Schema::dropIfExists('users_directories');
    }
}
