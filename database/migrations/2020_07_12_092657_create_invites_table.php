<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inviter_user_id');
            $table->string('token');
            $table->string('invitee_email');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('inviter_user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('invites_shared_directories', function (Blueprint $table) {
            $table->unsignedBigInteger('invite_id');
            $table->unsignedBigInteger('directories_id');

            $table->foreign('invite_id')->references('id')->on('invites')->onDelete('cascade');
            $table->foreign('directories_id')->references('id')->on('directories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invites');
        Schema::dropIfExists('invites_shared_directories');
    }
}
